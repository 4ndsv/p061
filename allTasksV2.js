var zeev = {};

window.addEventListener('DOMContentLoaded', (event) => {

    zeev.logger = {
        error: function (ex) {
            console.warn(ex);
        }
    };
    zeev.fn = {
        getUrlByEnv: function ([dev, hml, prd, env]) {
            try {
                switch (env) {
                    case 'HOMOLOG':
                        return hml;
                    case 'PROD':
                        return prd;
                    default:
                        return dev;
                }
            }
            catch (ex) {
                zeev.logger.error(ex);
            }
        },
        hideField: function (xname) {
            try {
                let tableRow = document.querySelector(`[xname=inp${xname}]`).closest('tr')
                if (tableRow)
                    tableRow.hidden = true;
            } catch (ex) {
                zeev.logger.error(ex);
            }
        },
        showField: function (xname) {
            try {
                let tableRow = document.querySelector(`[xname=inp${xname}]`).closest('tr')
                if (tableRow)
                    tableRow.hidden = false;
            } catch (ex) {
                zeev.logger.error(ex);
            }
        }

    };
    zeev.handler = {
        programAttachmentsValidation: function () {
            let programa = $('inp:programa').find('option:selected').text() == '' ?
                ($("#td1nomePrograma").text() == "" ? $("#td1programa").text() : $("#td1nomePrograma").text())
                : $('inp:programa').find('option:selected').text();
            let portaria = $('inp:varPortaria').val();
            zeev.integrations.carregaPrograma(programa, portaria);
            zeev.biz.programAttachmentsValidation($('inp:mecanismo').val(), programa, zeev.vars.tasksAliasToValidateAttachments);
        },
        preenchePortarias: function (obj) {
            //Registro de Objeto em conformidade com a Portaria Inmetro
            var options = [];
            var opt = null;
            $('inp:registroObjetoConformePortaria').find('option').not(':first').remove();

            for (var i = 0; i < obj.Portarias.length; i++) {
                var dataVigencia = obj.Portarias[i].DataVigenciaFabricação[0].DataInicialVigencia;
                var portaria = obj.Portarias[i].DocumentoLegal;
                portaria = portaria.split(' ');
                portaria = portaria[portaria.length - 1];

                if ($('#inpDsFlowElementAlias').val() !== 'T04A' && $('#inpDsFlowElementAlias').val() !== 'T06A') {
                    if (GLOBAL.dataPortaria === '' || invertDate(portaria) >= invertDate(GLOBAL.dataPortaria)) {
                        opt = new Option(obj.Portarias[i].DocumentoLegal, obj.Portarias[i].DocumentoLegal);
                        $(opt).attr('data', portaria);
                        $(opt).attr('IDEscopo', obj.Portarias[i].IDEscopo);
                        $(opt).attr('dataVigencia', dataVigencia);
                        $(opt).html(obj.Portarias[i].DocumentoLegal); //IE8
                        options.push(opt);
                    }
                } else {
                    opt = new Option(obj.Portarias[i].DocumentoLegal, obj.Portarias[i].DocumentoLegal);
                    $(opt).attr('data', portaria);
                    $(opt).attr('IDEscopo', obj.Portarias[i].IDEscopo);
                    $(opt).attr('dataVigencia', dataVigencia);
                    $(opt).html(obj.Portarias[i].DocumentoLegal); //IE8
                    options.push(opt);
                }
            }
            $('inp:registroObjetoConformePortaria').append(options);

            //Quando a combo so tem um item, seleciona ele
            if (obj.Portarias.length == 1) {
                $('inp:registroObjetoConformePortaria').prop('selectedIndex', 1);

                var data = $('inp:registroObjetoConformePortaria').find('option:selected').attr('data');
                $('inp:varPortaria').val(data);
            }
        }
    }
    zeev.biz = {
        programAttachmentsValidation: function (mecanismo, programa, tasksAliasToValidateAttachments) {
            try {
                if (zeev.vars.codFlow == zeev.vars.P061SolicitarRegistroCodFlow && tasksAliasToValidateAttachments.indexOf(zeev.vars.dsFlowElementAlias != -1)) {
                    debugger;
                    zeev.vars.attachments.all.forEach((attachment) => { zeev.fn.hideField(attachment) });
                    if (mecanismo == zeev.vars.mecanimosEnum.certificacao) {
                        zeev.vars.attachments.byCertificacao.forEach((attachment) => { zeev.fn.showField(attachment) });
                        zeev.vars.attachments.byPrograma.forEach((programaItem) => {
                            if (programaItem.programas.indexOf(programa) != -1) {
                                programaItem.anexos.forEach((attachment) => { zeev.fn.showField(attachment) });
                            }
                        });
                    }
                }
            } catch (ex) {
                zeev.logger.error(ex);
            }
        }
    };
    zeev.settings.publishDate = "||PUBLISH_DATE||",
    zeev.vars = {
        urlObterCertificado: zeev.fn.getUrlByEnv(['../applications/proxy.aspx?http://ws-prodcert-d.inmetro.gov.br/Certificado.svc/REST/ObterCertificadosPorCertificadorEscopo/?callback=?',
            '../applications/proxy.aspx?http://ws-prodcert-h.inmetro.gov.br/Certificado.svc/REST/ObterCertificadosPorCertificadorEscopo/?callback=?',
            '../applications/proxy.aspx?http://ws-prodcert.inmetro.gov.br/Certificado.svc/REST/ObterCertificadosPorCertificadorEscopo/?callback=?',
            ENVIRONMENT]),
        urlObterCNPJCertificado: zeev.fn.getUrlByEnv(['../applications/proxy.aspx?http://ws-prodcert-d.inmetro.gov.br/Certificado.svc/REST/v2ObterCertificadoPorCertificadorNumero/?callback=?',
            '../applications/proxy.aspx?http://ws-prodcert-h.inmetro.gov.br/Certificado.svc/REST/v2ObterCertificadoPorCertificadorNumero/?callback=?',
            '../applications/proxy.aspx?http://ws-prodcert.inmetro.gov.br/Certificado.svc/REST/v2ObterCertificadoPorCertificadorNumero/?callback=?',
            ENVIRONMENT]),
        urlDadosPrograma: zeev.fn.getUrlByEnv(['../applications/proxy.aspx?http://ws-registro-d.inmetro.gov.br/programa.svc/rest/v2/DadosPrograma/?',
            '../applications/proxy.aspx?http://ws-registro-h.inmetro.gov.br/programa.svc/rest/v2/DadosPrograma/?',
            '../applications/proxy.aspx?http://ws-registro.inmetro.gov.br/programa.svc/rest/v2/DadosPrograma/?',
            ENVIRONMENT
        ]),
        urlControlePortarias: '../applications/dipac/p061/Servicos/Portarias/ConsultarPortarias?',
        urlAPIRegistroObter: '../applications/proxyObterRegistro.aspx?',
        urlAPIRegistroObterInserindo: '../applications/proxyObterRegistroInserindoItens.aspx?',
        urlAPIRegistroEndereco: '../applications/proxyObterEndereco.aspx?',
        dataTypeCertificado: 'jsonp',
        codFlow: document.getElementById('inpCodFlow').value,
        dsFlowElementAlias: document.getElementById('inpDsFlowElementAlias').value,
        P061SolicitarRegistroCodFlow: 498,
        tasksAliasToValidateAttachments: ["T05", "T051", "T05.2", "T05A", "T05B", "T05A.1", "T05B.1", "T06A", "T06B", "T06C", "T04A_AdequarSolicitacaoDeRegistro", "T04B_AdequarSolicitacaoDeRegistro"],
        mecanimosEnum: { certificacao: 'Certificação' },
        attachments: {
            all: [
                'anexoAtestadoConformidade',
                'anexoAutorizacaoDetentorAtestado',
                'anexoAutorizacaoUsoMarca',
                'anexoPlanilhaEspecificacoesTecnicas',
                'anexoDocumentoEncerramento',
                'anexoRelatorioDeVerificacaoEmitidoPorMembroRbmlqi',
                'anexoPet',
                'anexoEnce',
                'anexoPlanilhaDeEficienciaEnergetica',
                'anexoRelatorioDeEnsaio',
                'anexoForDconf040',
                'anexoForDconf056',
                'anexoCertificado',
                'anexoCertificadoDeSegurancaDaNavegacaoCsn',
                'anexoMemorialDescritivoComFotoDoProdutoemPdf',
                'anexoFordconf056tarefaEmpresa',
                'anexoFordconf019tarefaOrgaoDelegado',
                'anexoPlanilhaTabelaDeEficienciaEnergetica'
            ],
            byCertificacao: ['anexoAtestadoConformidade'],
            byPrograma: [{
                'programas': ['Ventiladores de mesa, de coluna e circulador de ar'], 'anexos': ['anexoPet', 'anexoEnce', 'anexoPlanilhaTabelaDeEficienciaEnergetica']
            },
            {
                'programas': ['Brinquedos'], 'anexos': ['anexoMemorialDescritivoComFotoDoProdutoemPdf']
            },
            {
                'programas': ["Máquinas de Lavar Roupas de Uso Doméstico", "Ventilador de Teto de Uso Residencial"], 'anexos': ["anexoPet", "anexoEnce", "anexoPlanilhaDeEficienciaEnergetica", "anexoRelatorioDeEnsaio", "anexoForDconf056"]
            }
            ]
        }
    };
    zeev.integrations = {
        carregaPrograma(programa, portaria) {
            if (programa !== '') {
                var dados = {
                    codigo: programa
                }

                $.ajax({
                    type: 'GET',
                    url: zeev.vars.urlDadosPrograma,
                    async: false,
                    contentType: "application/json",
                    dataType: 'json',
                    data: dados,
                    success: function (json) {
                        zeev.handler.preenchePortarias(json);
                        $('inp:registroObjetoConformePortaria').find('option[value="' + portaria + '"]').attr('selected', 'selected');
                    },
                    error: function (ex) {
                        zeev.logger.error(ex)
                    }
                });
            }
        }
    };


    zeev.handler.programAttachmentsValidation();
});